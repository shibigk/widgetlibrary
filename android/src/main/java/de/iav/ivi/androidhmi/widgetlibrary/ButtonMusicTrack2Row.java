package de.iav.ivi.androidhmi.widgetlibrary;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

public class ButtonMusicTrack2Row extends LinearLayout {
    TextView artistText;
    TextView trackText;
    ImageView coverArtImage;

    public ButtonMusicTrack2Row(Context context, AttributeSet attrs) {
        super(context, attrs);
        init(context, attrs);
    }

    private void init(Context context, AttributeSet attrs) {
        inflate(context, R.layout.buttonmusictrack2row, this);
        int[] sets = {R.attr.artistText, R.attr.trackText};
        TypedArray typedArray = context.obtainStyledAttributes(attrs, sets);
        typedArray.recycle();
        initComponents();
    }

    private void initComponents() {
        artistText = (TextView) findViewById(R.id.artist_Text);
        trackText = (TextView) findViewById(R.id.track_Text);
    }

    public CharSequence getArtistText() {
        return artistText.getText();
    }

    public void setArtistText(CharSequence value) {
        artistText.setText(value);
    }

    public CharSequence getTrackText() {
        return trackText.getText();
    }

    public void setTrackText(CharSequence value) {
        trackText.setText(value);
    }

}
